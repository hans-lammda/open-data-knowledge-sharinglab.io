#!/bin/bash

# Build css
sass bootstrap-scss/custom.scss custom/custom.css
purgecss --css custom/custom.css --content ../public/*.html -o custom/custom-purged.css
minify custom/custom-purged.css > custom/custom-min.css
#cat ../stilmallar/custom/custom-min.css | jq -Rs '{"style":.}' > style.json
